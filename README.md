# README #

Para utilização dos dados em JSON:

1. faça o download do arquivo [bookmarks-docs.json](https://bitbucket.org/thiagobueno/mac5861-projeto2/src/3f5c812f4eeac8599d774c40be4f22d50679e16d/bookmarks-docs.json?at=master)

2. execute na linha de comando:
```
#!shell

$ mongoimport --db bookmarks --collection posts --file bookmarks-docs.json
```